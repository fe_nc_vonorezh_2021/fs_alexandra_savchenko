import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { PetModule } from "./modules/pet.module";

import { AppComponent } from "./app.component";
import { AppService } from "./app.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, PetModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
