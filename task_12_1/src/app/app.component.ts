import { Component } from "@angular/core";
import { AppService } from "./app.service";
import { Pet } from "./pet/pet";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.less"],
  providers: [AppService]
})
export class AppComponent {
  public btnText: string = "Скрыть котиков";
  public catsVisibility: boolean = true;
  public petsList: Pet[] = this.service.getPetsList();

  constructor(private service: AppService) {}

  public toggleCats() {
    this.catsVisibility = !this.catsVisibility;

    if (this.catsVisibility === false) {
      this.btnText = "Показать котиков";
      this.petsList = this.service.filterPetsList();
    } else {
      this.btnText = "Скрыть котиков";
      this.petsList = this.service.getPetsList();
    }
  }
}
