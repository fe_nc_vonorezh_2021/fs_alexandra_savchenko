import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PetListComponent } from './pet-list/pet-list.component';
import { PetInfoComponent } from './pet-info/pet-info.component';

@NgModule({
  declarations: [
    PetListComponent,
    PetInfoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PetListComponent
  ]
})
export class PetModule { }
