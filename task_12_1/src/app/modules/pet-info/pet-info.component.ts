import { Component, Input, OnInit, Output, EventEmitter, HostBinding } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Pet } from '../../pet/pet';

@Component({
  selector: 'app-pet-info',
  templateUrl: './pet-info.component.html',
  styleUrls: ['./pet-info.component.less'],
  providers: [AppService]
})
export class PetInfoComponent implements OnInit {  
  @Input()
  public pet: any;

  @Input()
  @HostBinding("class._active")
  public active: boolean = false;

  constructor(private service: AppService) {}

  public changeStatus(id: number): void {
    this.service.getPetById(id).isActive = !this.service.getPetById(id).isActive;
  }

  public checkStatus(id: number): boolean {
    return this.service.getPetById(id).isActive;
  }

  @Output()
  private onPetSelect: EventEmitter<number> = new EventEmitter();

  public ngOnInit(): void {
    console.log("ngOnInit", this.pet.name);
  }

  public _selectPet(): void {
    this.onPetSelect.emit(this.pet.id);
    this.pet.isActive = !this.pet.isActive;
  }

  public _isActive(): boolean {
    return this.pet.isActive;
  }

}
