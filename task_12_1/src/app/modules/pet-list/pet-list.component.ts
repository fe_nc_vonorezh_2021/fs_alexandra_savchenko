import { Component, Input} from '@angular/core';
import { PetInfoComponent } from '../pet-info/pet-info.component';
import { Pet } from '../../pet/pet';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html'
})
export class PetListComponent {
  @Input()
  public pets: Pet[] = [];

  public _activePetId: any;

  public _onPetSelect(id: number): void {
    this._activePetId = id;
  }
}
